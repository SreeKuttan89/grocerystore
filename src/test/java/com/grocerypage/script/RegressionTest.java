package com.grocerypage.script;

import java.io.IOException;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.grocerystore.pages.AddProductsToCart;
import com.grocerystore.pages.HomePage;
import com.grocerystore.pages.MyAccount;
import com.grocerystore.pages.NewCustomer;
import com.utitlies.ExcelUtility;

public class RegressionTest extends BaseClass
{
	@Test
	public void verifyPages() throws IOException
	{   //*********Read from excel grocery item******//
		ExcelUtility excel=new ExcelUtility("C:\\Users\\Sree\\Desktop\\grocery.xlsx", "Items");
		String firstItem=excel.getcellData(1, 0);
		
		//*********HomePage********//
		HomePage hp=new HomePage(driver,wait);
		String homeTitle=hp.verifyHomePageTitle();
		Assert.assertEquals(homeTitle,"Grocerystore");//check title
		boolean logoHp=hp.verifyHpLogo();    //check logo
		Assert.assertTrue(logoHp);
		hp.cickLogin();
		hp.searchItems(firstItem);
		
		/*********Read from excel User credentials******/
		ExcelUtility user=new ExcelUtility("C:\\Users\\Sree\\Desktop\\grocery.xlsx", "Credentials");
		String userEmail=user.getcellData(1, 0);
		String accPassword=user.getcellData(1, 1);
		//*******Account Login*************//
		NewCustomer nc=new NewCustomer(driver,wait);
		String accPageTitle=nc.verifyCurrentPageTitle();
		Assert.assertEquals(accPageTitle, "Account Login");
		boolean logoNC=nc.verifyNewCustLogo();    //check logo
		Assert.assertTrue(logoNC);
		nc.enterReturningUserDetails(userEmail, accPassword);
		
		//************My Account Validation*********//
		MyAccount myac=new MyAccount(driver,wait);
		String myacc=myac.verifyAccPageTitle();
		Assert.assertEquals(myacc, "My Account");
		boolean logoAc=myac.verifyAccountPageLogo();   //check logo
		Assert.assertTrue(logoAc);
		AddProductsToCart addP=myac.clickHomePage();
		String tile=addP.verifyPageTitle();
		Assert.assertEquals(tile, "Spoon");
		boolean logoCart=addP.verifyCartLogo();  //check logo
		Assert.assertTrue(logoCart);
		addP.selectItem1();
		addP.enterSearch(firstItem);
		//addP.goToCart();
	}
}
