package com.grocerypage.script;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeMethod;

import com.utitlies.NotepadUtility;

public class BaseClass {
	WebDriver driver;
	WebDriverWait wait;

	public void LaunchBrowser(String Browser) {
		if (Browser.equalsIgnoreCase("Chrome")) {
			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\Sree\\eclipse-workspace\\NewSelenium\\Lib\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			wait = new WebDriverWait(driver, 60);
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
		} else {
			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\Sree\\eclipse-workspace\\NewSelenium\\Lib\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			wait = new WebDriverWait(driver, 90);
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
		}

	}

	@BeforeMethod

	public void launch() {
		LaunchBrowser("chrome");
		driver.get(NotepadUtility.getUrl());
		// driver.get("http://www.opesmount.in/grocerystore1/");
	}

	// @AfterMethod
	public void closeBrowser() {
		driver.close();
	}
}
