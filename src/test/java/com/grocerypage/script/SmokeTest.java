package com.grocerypage.script;

import java.io.IOException;

import org.junit.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.grocerystore.pages.AddProductsToCart;
import com.grocerystore.pages.HomePage;
import com.grocerystore.pages.MyAccount;
import com.grocerystore.pages.NewCustomer;
import com.grocerystore.pages.ShoppingCart;
import com.utitlies.ExcelUtility;
import com.utitlies.ListenerName;
import com.utitlies.PageUtility;

@Listeners(ListenerName.class)
public class SmokeTest extends BaseClass
{
	@Test
	public void verifyPages() throws IOException
	{   //*********Read from excel grocery item******//
		ExcelUtility excel=new ExcelUtility("C:\\Users\\Sree\\Desktop\\grocery.xlsx", "Items");
		String firstItem=excel.getcellData(1, 0);
		
		/***************************************HomePage*****************************************/
		HomePage hp=new HomePage(driver,wait);
		String homeTitle=hp.verifyHomePageTitle();
		
		Assert.assertEquals(homeTitle,"Grocerystore");
		PageUtility.takeScreenshots(driver);
		hp.cickLogin();
		hp.searchItems(firstItem);
		
		/***************************Read from excel User credentials*********************/
		ExcelUtility user=new ExcelUtility("C:\\Users\\Sree\\Desktop\\grocery.xlsx", "Credentials");
		String userEmail=user.getcellData(1, 0);
		String accPassword=user.getcellData(1, 1);
		/*******************************Account Login******************************/
		NewCustomer nc=new NewCustomer(driver,wait);
		String accPageTitle=nc.verifyCurrentPageTitle();
		Assert.assertEquals(accPageTitle, "Account Login");
		nc.enterReturningUserDetails(userEmail, accPassword);

		/****************************** My Account Validation ************************/
		MyAccount myac = new MyAccount(driver, wait);
		String myacc = myac.verifyAccPageTitle();
		Assert.assertEquals(myacc, "My Account");
		AddProductsToCart addP = myac.clickHomePage();
		String tile = addP.verifyPageTitle();
		Assert.assertEquals(tile, "Spoon");
		addP.selectItem1();
		addP.enterSearch(firstItem);
		addP.goToCart();

		/**********************************
		 * Shopping Cart
		 ******************************/

		ShoppingCart sc = new ShoppingCart(driver, wait);
		sc.verifyshoppingTitle();
		sc.removeProduct();
		sc.continueCheckout();

	}
}
