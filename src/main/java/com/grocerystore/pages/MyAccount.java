package com.grocerystore.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utitlies.PageUtility;

public class MyAccount 
{
	WebDriver driver;
	WebDriverWait wait;
	@FindBy(xpath="//a[contains(text(),'Product ')]")
	WebElement homeLink;
	@FindBy(xpath="//div[@class='col-sm-4 slick-slide slick-cloned slick-active']//div[1]//div[2]")
	WebElement shopNow;
	public MyAccount(WebDriver driver,WebDriverWait wait)
	{
		this.driver=driver;
		this.wait=wait;	
		PageFactory.initElements(driver, this);
	}
	public String verifyAccPageTitle()
	{
		String myAcctPageTitle=PageUtility.getPageTitle(driver);
		return myAcctPageTitle;
	}
	public boolean verifyAccountPageLogo()
	{
		boolean logoAcc=PageUtility.logoVerification(driver);
		return logoAcc;
	}
	public AddProductsToCart clickHomePage()
	{
		homeLink.click();
		shopNow.click();
		return new AddProductsToCart(driver,wait);
	}
}
