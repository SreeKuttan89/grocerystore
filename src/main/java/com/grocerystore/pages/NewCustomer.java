package com.grocerystore.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utitlies.PageUtility;

public class NewCustomer 
{
	WebDriver driver;
	WebDriverWait wait;
	@FindBy(xpath="//a[contains(text(),'Continue')]")
	WebElement continueButton;
	@FindBy(xpath="//input[@name='email']")
	WebElement email;
	@FindBy(xpath="//input[@name='password']")
	WebElement password;
	@FindBy(xpath="//input[@type='submit']")
	WebElement loginButton;
	public NewCustomer(WebDriver driver,WebDriverWait wait)
	{
		this.driver=driver;
		this.wait=wait;	
		PageFactory.initElements(driver, this);
	}
	public String verifyCurrentPageTitle()
	{
		String acctPageTitle=PageUtility.getPageTitle(driver);
		return acctPageTitle;
	}
	public boolean verifyNewCustLogo()
	{
		boolean logoNew=PageUtility.logoVerification(driver);
		return logoNew;
	}
	public void clickContinueButton()
	{
		continueButton.click();
	}
	public void enterReturningUserDetails(String emailid,String pass)
	{
		email.sendKeys(emailid);
		password.sendKeys(pass);
		loginButton.click();
		
	}
}
