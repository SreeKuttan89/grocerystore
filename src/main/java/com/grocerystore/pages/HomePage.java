package com.grocerystore.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utitlies.PageUtility;


public class HomePage 
{
	WebDriver driver;
	WebDriverWait wait;
	@FindBy(xpath="//a[contains(text(),'Login')]")
	WebElement login;
	@FindBy(xpath="//input[@type='text']")
	WebElement enterSearchitem;
	@FindBy(xpath="//input[@type='text']")
	WebElement searchitem;
	
	public HomePage(WebDriver driver,WebDriverWait wait)
	{
		this.driver=driver;
		this.wait=wait;	
		PageFactory.initElements(driver, this);
	}
	/******Verify Page Title*****/
	public String verifyHomePageTitle()
	{
		
		String hpTitle=PageUtility.getPageTitle(driver);
		return hpTitle;
		
	}
	/*********************Logo Check************/
	public boolean verifyHpLogo()
	{
		boolean logo=PageUtility.logoVerification(driver);
		return logo;
	}
	public void cickLogin()
	{
		wait.until(ExpectedConditions.visibilityOf(login)).click();
	}
	public void searchItems(String item)
	{
		enterSearchitem.sendKeys(item);
		searchitem.click();
		
	}
}
