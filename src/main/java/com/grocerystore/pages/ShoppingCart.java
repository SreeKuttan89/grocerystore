package com.grocerystore.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utitlies.PageUtility;

public class ShoppingCart 
{
	WebDriver driver;
	WebDriverWait wait;
	@FindBy(xpath="//td//div//span//button[contains(@data-original-title,'Remove')]//i")
	WebElement remove;
	@FindBy(xpath="//a[contains(text(),'Checkout')]")
	WebElement checkoutButton;
	public ShoppingCart(WebDriver driver,WebDriverWait wait)
	{
		this.driver=driver;
		this.wait=wait;	
		PageFactory.initElements(driver, this);
	}
	public String verifyshoppingTitle()
	{
		String mycurrentPageTitle=PageUtility.getPageTitle(driver);
		return mycurrentPageTitle;
	}
	public void removeProduct()
	{
		PageUtility.isElementClickable(remove, wait, driver);
		
	}
	public void continueCheckout()
	{
		PageUtility.isElementClickable(checkoutButton, wait, driver);
	}
}
