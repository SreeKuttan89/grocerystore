package com.grocerystore.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.utitlies.PageUtility;

public class AddProductsToCart 
{
	WebDriver driver;
	WebDriverWait wait;
	@FindBy(xpath="//a//img[@title='Dried Apple']")
	WebElement item1;
	@FindBy(xpath="//button[@id='button-cart']")
	WebElement addToCart1;
	@FindBy(xpath="//input[@name='search']")
	WebElement search;
	@FindBy(xpath="//span[@class='input-group-btn']//button")
	WebElement clickSearch;
	@FindBy(xpath="//div[@class='col-lg-4 col-xs-12 products']//a//img")
	WebElement addSearchItem;
	@FindBy(xpath="//div[@id='cart']//button")
	WebElement clickCartIcon;
	@FindBy(xpath="//a//strong[contains(text(),' Checkout')]")
	WebElement viewCart;
	@FindBy(xpath="//a[contains(text(),'shopping')]")
	WebElement clickShoppingCart;
	public AddProductsToCart(WebDriver driver,WebDriverWait wait)
	{
		this.driver=driver;
		this.wait=wait;	
		PageFactory.initElements(driver, this);
	}
	public String verifyPageTitle()
	{
		String mycurrentPageTitle=PageUtility.getPageTitle(driver);
		return mycurrentPageTitle;
	}
	public boolean verifyCartLogo()
	{
		boolean logoC=PageUtility.logoVerification(driver);
		return logoC;
	}
	public void selectItem1()
	{
		addToCart1.click();
	}
	public void enterSearch(String itemName)
	{
		search.sendKeys(itemName);
		clickSearch.click();
		addSearchItem.click();
		addToCart1.click();
	}
	public void goToCart()
	{
		
		  ((JavascriptExecutor)driver).executeScript("scroll(0,400)");
		  PageUtility.isElementClickable(clickCartIcon, wait, driver);
		  ((JavascriptExecutor)driver).executeScript("scroll(0,400)");
		  PageUtility.isElementClickable(viewCart, wait, driver);
		 
		/*
		 * ((JavascriptExecutor)driver).executeScript("scroll(0,700)");
		 * driver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
		 * clickShoppingCart.click();
		 */
		
	}
}
