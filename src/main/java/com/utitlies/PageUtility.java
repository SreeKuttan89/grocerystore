package com.utitlies;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.commons.io.FileUtils;

public class PageUtility {
	public static WebDriver driver;
	public WebDriverWait wait;
	public Actions actions;
	static By logo = By.xpath("//div[@class='row']//div[@class='col-lg-4 col-md-6 col-sm-12  logo']//a//img");
	static By aboutInfo = By.xpath("//div[@class='row']//div//h3[contains(text(),'About Goceryztore')]");
	static By externalLinks = By.xpath("//div[@class='row']//div[2]//h3[contains(text(),'Extra Links')]");

	// Method for Taking Screenshot
	public static void takeScreenshots(WebDriver driver)
	{ 
		// 1.Take screenshot and storing it as file
		File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// 2.copy the screenshot to particular folder
		File DestFile=new File("C:\\\\Users\\\\Sree\\\\eclipse-workspace\\\\groceryStore\\1.jpg");
		try 
		{
			FileUtils.copyFile(file, DestFile);
		} catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//FileHandler.copy(file, new File("C:\\Users\\Sree\\eclipse-workspace\\groceryStore" + fileName + ".jpg"));
	}

	// get title of pages
	public static String getPageTitle(WebDriver driver) 
	{
		try
		{
			System.out.print(String.format("The title of the page is: %s\n\n", driver.getTitle()));
			return driver.getTitle();
		} catch (Exception e) 
		{
			System.out.println("Print the Exception::" + e);
		}
		return null;
	}

	// Verify header logo
	public static boolean logoVerification(WebDriver driver) 
	{
		try
		{
			boolean logoPresent = driver.findElement(logo).isDisplayed();
			System.out.println("If logo available then true/else flase::" + logoPresent);
			return logoPresent;
		} catch (Exception e) {
			System.out.println("No such logo in the page::" + e);
		}
		return false;
	}

	// Verify Footer About US Info
	public static boolean footerAboutUsInfo(WebDriver driver) {
		try
		{
			boolean aboutus = driver.findElement(aboutInfo).isDisplayed();
			System.out.println("If logo available then true/else flase::" + aboutus);
			return aboutus;
		} catch (Exception e) {
			System.out.println("No such Info about the page::" + e);
		}
		return false;
	}

	// Verify External links in Footer
	public static boolean externalLinks(WebDriver driver) 
	{
		try 
		{
			boolean extLinks = driver.findElement(externalLinks).isDisplayed();
			System.out.println("If logo available then true/else flase::" + extLinks);
			return extLinks;
		} catch (Exception e) {
			System.out.println("No such Link on the footer section::" + e);
		}
		return false;
	}
	
	public static void isElementClickable(WebElement we,WebDriverWait wait,WebDriver driver) 
	{
		try 
		{
			wait.until(ExpectedConditions.elementToBeClickable(we));
			we.click();
		} 
		catch (Exception e) 
		{
			System.out.println("No such elemlent is avaliable to click::" + e);
		}
		
	}
	
	
}
