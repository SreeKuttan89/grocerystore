package com.utitlies;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtility 
{
	FileInputStream file;
	XSSFWorkbook workbook;
	XSSFSheet sheet;
	XSSFRow row;
	XSSFCell cell;

	public ExcelUtility(String filepath,String sheetname) throws IOException
	{
		file= new FileInputStream(filepath);
		workbook=new XSSFWorkbook(file);
		sheet =workbook.getSheet(sheetname);
		
	}

	public String getcellData(int rowNo, int cellNo) {
		row = sheet.getRow(rowNo);
		cell = row.getCell(cellNo);
		String celldata = cell.getStringCellValue();
		return celldata;
	}

}
